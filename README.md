Create Disks
============

Create a disk (filesystem + mount) on the right volume

Requirements
------------

The only requirement is to have a disk list which describe the purpose of the
disk in a server in this form:

```yaml
disks:
  - name: disk-XYZ
  - name: disk2
  - name: disk-db
```

server must use `systemd` for the services. (Should) works on `apt` and `yum`
based systems (debian, ubuntu, centos, rhel, ...).

Role Variables
--------------

As said on requirements, the `disks` list must be given.

Other variables are:

- `disk_purpose`(mandatory): the "purpose" of the disk (and how to retrieve
  it from the disk list). On the example above, it can be 'db' to take the
  third disk.
- `mount_path`(default to `/mnt`): where to mount the disk
- `fstype`(default to `xfs`): what type of filesystem to use. filesystems
  supported are `xfs`, `ext2` ,`ext3` ,`ext4`, `btrfs`.
- `proxy_env` if you want to set proxy informations (http_proxy, https_proxy, ...)
- `force_full_erase` (default to `True`) makes role to unmount / wipe / remount
   the path. If you don't need it, you can set it to `False`.

Dependencies
------------

the following modules are needed:

- mount (https://docs.ansible.com/ansible/latest/modules/mount_module.html);
- systemd (https://docs.ansible.com/ansible/latest/modules/systemd_module.html);
- parted (https://docs.ansible.com/ansible/latest/modules/parted_module.html)
  and thus `parted` must be installed on the target server.

`dd` is also used on the target server and it then must also be present.

Example Playbook
----------------

```yaml
- hosts: servers
  pre_tasks:
    - set_fact:
        disks:
          - name: disk-XYZ
          - name: disk2
          - name: disk-db
  roles:
    - role: create_disk
      disks: "{{ disks }}"
      disk_purpose: db
      mount_path: /var/lib/db
```

License
-------

Apache 2.0
